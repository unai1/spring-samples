/**
 * 
 */
package org.unaiinigo.javakartspring;

import java.util.Random;

/**
 * This is a class to create a cart
 * @author Unai
 *
 */
public class Cart {
	private int speed = 0;
	private String name = "";
	private int aceleration = 0;
	private int grip = 0;
	private int position = 0;
	/**
	 * This is a method to give to the cart some parameters
	 */
	public void init(){
		Random random = new Random();
		this.position = 0;
		this.speed = random.nextInt(18);
		this.aceleration = random.nextInt(18 - this.speed);
		this.grip = random.nextInt(18 - this.speed - this.aceleration);
	}
	/**
	 * This is a method to move the car
	 * @return result
	 */
	public int move(){
		int result = 0;
		Random random = new Random();
		result = this.speed + this.aceleration + random.nextInt(6);
		return result;
	}
	/**
	 * This is a method to calculate the handle of the car
	 * @return result
	 */
	public int handle(){
		int result = 0;
		Random random = new Random();
		result = this.grip + random.nextInt(6);
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Cart [speed=" + speed + ", name=" + name + ", aceleration="
				+ aceleration + ", grip=" + grip + ", position=" + position
				+ "]";
	}
	/**
	 * get of the class for the name
	 * @return name
	 */
	public String getName(){
		return name;
	}
	/**
	 * This is the set for the name
	 * @param name
	 */
	public void setName(String name){
		this.name = name;
	}
	/**
	 * This is the get for the speed
	 * @return speed
	 */
	public int getSpeed(){
		return speed;
	}
	/**
	 * @return the aceleration
	 */
	public int getAceleration() {
		return aceleration;
	}
	/**
	 * @param aceleration the aceleration to set
	 */
	public void setAceleration(int aceleration) {
		this.aceleration = aceleration;
	}
	/**
	 * @return the grip
	 */
	public int getGrip() {
		return grip;
	}
	/**
	 * @param grip the grip to set
	 */
	public void setGrip(int grip) {
		this.grip = grip;
	}
	/**
	 * @return the position
	 */
	public int getPosition() {
		return position;
	}
	/**
	 * @param position the position to set
	 */
	public void setPosition(int position) {
		this.position = position;
	}
	
}
