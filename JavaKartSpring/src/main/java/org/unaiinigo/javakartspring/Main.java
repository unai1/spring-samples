/**
 * 
 */
package org.unaiinigo.javakartspring;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author Unai
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		 ApplicationContext context = new ClassPathXmlApplicationContext("javaCart.xml");
	        
	        Race race = (Race) context.getBean("race");
	        
	        System.out.println(race.toString());
	        race.run();
	        System.out.println(race.showResult());

	}

}
