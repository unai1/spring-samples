/**
 * 
 */
package org.unaiinigo.javakartspring;

import java.util.Vector;

/**
 * This is a class that simulate the race
 * @author Unai
 *
 */
public class Race {
	private String name = "";
	private Circuit circuit;
	private Vector<Cart>carts = new Vector<Cart>();
	private Vector<Cart>finish = new Vector<Cart>();
	/**
	 * The default constructor of the class
	 */
	public Race(){
		
	}
	/**
	 * This do to run the car
	 */
	public void run(){
		do{
			for(int i = 0; i<carts.size();i++){
				moveCart(carts.elementAt(i));
				if(isFinished(carts.elementAt(i))){
					moveToFinished(carts.elementAt(i));
				}
			}
		}while(notAllFinished());
	}
	/**
	 * This is a class that show you the result
	 * @return result
	 */
	public String showResult(){
		String result = "Race: " + this.getName();
		result += " Circuit: " + circuit.toString();
		for(int i = 0; i<finish.size();i++){
			result += (i) + ".-" +finish.elementAt(i).toString() + "\n";
		}
		return result;
	}
	/**
	 * This method calculate the cart movement
	 * @param cart
	 */
	private void moveCart(Cart cart){
		int movePositions = 0;
		movePositions = cart.move() - (circuit.difficulty() - cart.handle());
		cart.setPosition(cart.getPosition() + movePositions);
	}
	/**
	 * move for the vector of race to the finish vector
	 * @param cart
	 */
	private void moveToFinished(Cart cart){
		finish.add(cart);
		carts.removeElement(cart);
	}
	/**
	 * This say if you have finish the race
	 * @param cart
	 * @return
	 */
	private boolean isFinished(Cart cart){
		return cart.getPosition() >= circuit.getDistance();
	}
	/**
	 * This is to say if all the carts have finish the race
	 * @return
	 */
	private boolean notAllFinished(){
		return carts.size() > 0;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Race [name=" + name + ", circuit=" + circuit + ", carts="
				+ carts + ", finish=" + finish + "]";
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the circuit
	 */
	public Circuit getCircuit() {
		return circuit;
	}
	/**
	 * @param circuit the circuit to set
	 */
	public void setCircuit(Circuit circuit) {
		this.circuit = circuit;
	}
	/**
	 * @return the carts
	 */
	public Vector<Cart> getCarts() {
		return carts;
	}
	/**
	 * @param carts the carts to set
	 */
	public void setCarts(Vector<Cart> carts) {
		this.carts = carts;
	}
	/**
	 * @return the finish
	 */
	public Vector<Cart> getFinish() {
		return finish;
	}
	/**
	 * @param finish the finish to set
	 */
	public void setFinish(Vector<Cart> finish) {
		this.finish = finish;
	}
	
	
}
