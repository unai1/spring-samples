/**
 * 
 */
package org.unaiinigo.javakartspring;

import java.util.Random;

/**
 * This is a class to create a circuit
 * @author Unai
 *
 */
public class Circuit {
	private String name = "";
	private int distance = 0;
	/**
	 * This is the empty constructor of the circuit
	 */
	public Circuit(){
		
	}
	/**
	 * This is the constructor with parameters of the class
	 * @param name
	 * @param distance
	 */
	public Circuit(String name, int distance){
		this.name = name;
		this.distance = distance;
	}
	/**
	 * This is a method to the difficulty of the circuit
	 * @return difficulty
	 */
	public int difficulty(){
		Random random = new Random();
		if(random.nextInt(3) == 0){
			return random.nextInt(5);
		}else{
			return 0;
		}
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Circuit [name=" + name + ", distance=" + distance + "]";
	}
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * @return the distance
	 */
	public int getDistance() {
		return distance;
	}
	/**
	 * @param distance the distance to set
	 */
	public void setDistance(int distance) {
		this.distance = distance;
	}
}
